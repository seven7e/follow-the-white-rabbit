#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter
import sys
import itertools
import hashlib
from multiprocessing import Pool
from itertools import islice
import pickle as pkl
import re

NUM_CORE = 8

def iter_chunks(data, num=8):
    size = int((len(data) + 7) / num)
    assert(num * size >= len(data))
    it = iter(data)
    for i in range(0, len(data), size):
        yield {k:data[k] for k in islice(it, size)}

def chunks(*a, **k):
    return list(iter_chunks(*a, **k))

target_md5 = [
        "e4820b45d2277f3844eac66c903e84be",
        "23170acc097c24edb98fc5488ab033fe",
        "665e5bcb0c20062fe8abaaf4628bb154"
        ]

def map_char2id(anagram):
    char2id = {}
    id2char = []
    for c in anagram:
        if c == ' ':
            continue
        if c not in char2id:
            char2id[c] = len(id2char)
            id2char.append(c)
    return char2id, id2char

def word2id(word, char2id):
    return [char2id[c] for c in word]

def words2id(words, char2id):
    return [word2id(w, char2id) for w in words]

def get_cnt(anagram, char2id):
    cnt = [0] * (max(char2id.values()) + 1)
    for c in anagram:
        if c == ' ':
            continue
        id = char2id[c]
        cnt[id] += 1
    return b''.join([c.to_bytes(1, 'little') for c in cnt])

def add_cnt(cnt1, cnt2):
    return b''.join([(cnt1[i]+cnt2[i]).to_bytes(1, 'little') for i in range(len(cnt1))])

def get_cnt_words(words, char2id):
    cnt = get_cnt(words[0], char2id)
    for w in words[1:]:
        cnt = add_cnt(cnt, get_cnt(w, char2id))
    return cnt

def setup(anagram, words, word_map):
    print(len(words))
    print(words[:5])

    #chars = set(anagram)
    #chars.remove(' ')
    #print(chars, len(chars))

    char2id, id2char = map_char2id(anagram)
    print(char2id, id2char)

    valid_words = [w for w in words
            if all(c in char2id for c in w)]
    print(len(valid_words))

    word_ids = words2id(valid_words, char2id)
    print(word_ids[:5])

    ana_counter = get_cnt(anagram, char2id)
    print('anagram counter:', len(ana_counter), ana_counter)

    char_counters = [get_cnt(w, char2id) for w in valid_words]
    print(char_counters[:5])
    return char2id, id2char, ana_counter, valid_words, char_counters

def _dump(obj, name):
    fpath = 'dump/' + name
    print('dump to file', fpath)
    with open(fpath, 'wb') as f:
        pkl.dump(obj, f)

def _load(name):
    fpath = 'dump/' + name
    print('load from file', fpath)
    with open(fpath, 'rb') as f:
        return pkl.load(f)

def find(anagram, words, word_map, phase):
    if phase <= -1:
        char2id, id2char, ana_counter, valid_words, char_counters = \
                setup(anagram, words, word_map)

        can_words = [(valid_words[i], char_counters[i]) \
                for i, cnt in enumerate(char_counters) \
                if maybe_candidate(cnt, ana_counter)]
        can_words.sort(key=lambda w: w[0])
        print(len(can_words))
        print(can_words[:5])

        can_wlist = []
        can_wmap = {}
        for i, (w, c) in enumerate(can_words):
            can_wlist.append(w)
            can_wmap.setdefault(c, []).append(w)

        _dump((char2id, id2char, ana_counter, valid_words, char_counters, \
                can_words, can_wlist, can_wmap), 'setup.pkl')
    else:
        char2id, id2char, ana_counter, valid_words, char_counters, \
                can_words, can_wlist, can_wmap = _load('setup.pkl')

    print('number of unique counts:', len(can_wmap))
    #uniq_word_cnts = sorted(can_wmap.keys())

    if phase <= 0:
        print('========== level 0 ==========')
        cnt0 = {zero_cnt(ana_counter): [tuple([])]}
        print('level 0:', cnt0)
        _dump(cnt0, 'cnt0.pkl')
    else:
        cnt0 = None

    cntn = cnt0
    for ph in range(max(1, phase), 5):
        print('========== level {} =========='.format(ph))
        if phase == ph:
            cntn1 = _load('cnt{}.pkl'.format(ph-1))
            print('previous level {}: {}'.format(ph-1, len(cntn1)))
        else:
            cntn1 = cntn
        cntn = find_n_tuple(cntn1, can_wlist, can_wmap, ana_counter, word_map, char2id)
        #cntn = {}
        print('level {}: {}'.format(ph, len(cntn)))
        _dump(cntn, 'cnt{}.pkl'.format(ph))
    return

    if phase <= 2:
        if phase == 2:
            cnt1 = _load('cnt1.pkl')
            print('level 1:', len(cnt1))
        print('========== level 2 ==========')
        cnt2 = find_n_tuple(cnt1, can_wlist, can_wmap, ana_counter, word_map, char2id)
        print('level 2:', len(cnt2))
        _dump(cnt2, 'cnt2.pkl')

    if phase <= 3:
        if phase == 3:
            cnt2 = _load('cnt2.pkl')
            print('level 2:', len(cnt2))
        print('========== level 3 ==========')
        cnt3 = find_n_tuple(cnt2, can_wlist, can_wmap, ana_counter, word_map, char2id)
        print('level 3:', len(cnt3))
        _dump(cnt3, 'cnt3.pkl')

    if phase <= 4:
        if phase == 4:
            cnt3 = _load('cnt3.pkl')
            print('level 3:', len(cnt3))
        print('========== level 4 ==========')
        cnt4 = find_n_tuple(cnt3, can_wlist, can_wmap, ana_counter, word_map, char2id)
        print('level 4:', len(cnt4))
        _dump(cnt4, 'cnt4.pkl')
    else:
        cnt4 = _load('cnt4.pkl')


def zero_cnt(ana_counter):
    return (b''.join([(0).to_bytes(1, 'little')] * len(ana_counter)))
def maybe_candidate(cnt, ana_counter):
    if len(cnt) != len(ana_counter):
        raise ValueError('')
    return all(n <= ana_counter[i] for i, n in enumerate(cnt))

def is_candidate(cnt, ana_counter):
    if len(cnt) != len(ana_counter):
        raise ValueError('')
    return all(n == cnt[i] for i, n in enumerate(ana_counter))


def _task(args):
    tid, cnt_n1_task, can_wlist, can_wmap, ana_counter, word_map = args
    print('====== process {}, {} items ======='.format(tid, len(cnt_n1_task)))

    cnt_n_task = {}
    def _gen_n_tuples(cnt, wid_tuples, wids):
        #print('--------------', wid_tuples, wids)
        cnt_n_task.setdefault(cnt, []).append((wid_tuples, wids))

        #for wt in wid_tuples:
        #    for w in wids:
        #        #if True:
        #        if len(wt) == 0 or w >= wt[-1]:
        #            #print(wt, w)
        #            cnt_n_task.setdefault(cnt, []).append(wt + (w,))

    for i, (t_cnt, wid_tuples) in enumerate(cnt_n1_task.items()):
        #print('cnt_n1_task', t_cnt, wid_tuples)
        if i % 1000 == 0: print('-- i --:', tid, i)
        for wcnt, wids in can_wmap.items():
            cnt = add_cnt(t_cnt, wcnt)
            if cnt == ana_counter:
                #print('preprint candidates:',
                #    #cnt,
                #    len(wid_tuples), '*', len(wids),
                #    sorted(set(re.split('[^a-z]+', str(wid_tuples) + ' ' + str(wids)))))
                #print(wid_tuples)
                #print(wids)
                _gen_n_tuples(cnt, wid_tuples, wids)
            if maybe_candidate(cnt, ana_counter):
                _gen_n_tuples(cnt, wid_tuples, wids)
                #if is_candidate(cnt, ana_counter):
                #check_result2(cnt, cnt_n_task[cnt], word_map, anagram)
    return cnt_n_task

def _merge_results(cnt_n_chunks):
    out = {}
    def _merge2(cnts1, cnts2):
        for k, v in cnts2.items():
            cnts1.setdefault(k, v).extend(v)
    for ck in cnt_n_chunks:
        _merge2(out, ck)
    return out

def find_n_tuple(cnt_n1, can_wlist, can_wmap, ana_counter, word_map, char2id):

    if len(cnt_n1) <= 1000:
        print('single processing')
        cnt_n = _task((0, cnt_n1, can_wlist, can_wmap, ana_counter, word_map))
    else:
        cnt_n1_chunks = iter_chunks(cnt_n1, NUM_CORE)
        args = [(i, c, can_wlist, can_wmap, ana_counter, word_map) for i, c in enumerate(cnt_n1_chunks)]
        print('num chunks:', len(args))
        with Pool(NUM_CORE) as p:
            cnt_n_chunks = p.map(_task, args)
        print('num output chunks:', len(cnt_n_chunks))
        #print(cnt_n_chunks[2])
        cnt_n = _merge_results(cnt_n_chunks)

    print('total', len(cnt_n))
    print('sample(max num words for one group: {})' \
            .format(max(len(ws) for _, ws in cnt_n.items())))
    for i, (cnt, ws) in enumerate(cnt_n.items()):
        print('\t{}\t{}\t{}'.format(cnt, len(ws), str(ws)[:100]))
        if i >= 5:
            break
    if ana_counter in cnt_n:
        print('========== found matches, start to check ===========')
        check_result2(ana_counter, cnt_n[ana_counter], word_map, char2id)
    return cnt_n

    bi = {}
    i = j = 0
    while i < len(uniq_word_cnts):
        if i % 100 == 0: print('-- i --:', i)
        cnt1 = uniq_word_cnts[i]
        j = i
        while j < len(uniq_word_cnts):
            cnt2 = uniq_word_cnts[j]
            #cnt1 = char_counters[w1]
            #cnt2 = char_counters[w2]
            #cnt = cnt1 + cnt2
            cnt = add_cnt(cnt1, cnt2)
            if maybe_candidate(cnt):
                bi.setdefault(cnt, set()).add((i, j))
                if is_candidate(cnt):
                    print('bi:', can_words[iw1], can_words[iw2])
            j += 1
        i += 1
    print('bi', len(bi))

    tri = []
    i = j = 0
    while i < len(bi):
        iw11, iw12, cnt1 = bi[i]
        if i % 1000 == 0: print('-- i --:', i, iw11, iw12)
        j = iw12
        while j < len(uni):
            iw2, cnt2 = uni[j]
            #cnt1 = char_counters[w1]
            #cnt2 = char_counters[w2]
            #cnt = cnt1 + cnt2
            cnt = add_cnt(cnt1, cnt2)
            if maybe_candidate(cnt):
                #tri.append((i, j))
                if is_candidate(cnt):
                    wc1, wc2, wc3 = can_words[iw11], can_words[iw12], can_words[iw2]
                    print('tri:', iw11, iw12, iw2, wc1, wc2, wc3)
                    check_result([w for w, c in [wc1, wc2, wc3]], word_map)
            j += 1
        i += 1
    print('tri', len(tri))


def check_result(words, word_map):
    def _get_ori_word(words):
        if len(words) == 0:
            yield []
        else:
            for w in word_map[words[0]]:
                for suf in _get_ori_word(words[1:]):
                    yield [w] + suf

    for wb in itertools.permutations(words):
        for wo in _get_ori_word(wb):
            wt = ' '.join(wo)
            print('checking:', wt, wo)
            try:
                dig = hashlib.md5(wt.encode('ascii')).hexdigest()
            except UnicodeEncodeError:
                print('!!!!!!! failed to encode, skip', wt, wo)
                continue
            print('\tmd5:', dig)
            if dig in target_md5:
                print('---------------- found solution ------------------')
                print(wt, wo, dig)
                print('--------------------------------------------------')

def gen_words(can_gen):
    visited = set()
    for cg in can_gen:
        assert(type(cg) == tuple)
        if len(cg) == 0:
            yield ()
        else:
            pre_can_gen, words = cg
            for pw in gen_words(pre_can_gen):
                for w in words:
                    wg = tuple(sorted(pw + (w,)))
                    if wg not in visited:
                        visited.add(wg)
                        yield wg

def check_result2(cnt, can_gen, word_map, char2id):
    print('############# check result ############', len(can_gen))
    for i, words in enumerate(gen_words(can_gen)):
        print('####### check for word collection {} #######'.format(words))
        assert(cnt == get_cnt_words(words, char2id))
        check_result(words, word_map)

def main():
    anagram = 'poultry outwits ants'
    fpath = './worduniq'
    #fpath = './wordtest'
    print(anagram)

    words = [s.strip() for s in open(fpath)]
    print('nb words:', len(words))

    r_words = [''.join(c for c in s if c >= 'a' and c <= 'z') for s in words]
    print('nb words removing none letters:', len(r_words))

    word_map = {}
    for ws, w in zip(r_words, words):
        word_map.setdefault(ws, set()).add(w)

    words = sorted(set(r_words))
    print('nb words after unique:', len(words))

    if len(sys.argv) > 1:
        phase = int(sys.argv[1])
    else:
        phase = 4
    find(anagram, words, word_map, phase)
    return

if __name__ == '__main__':
    main()
