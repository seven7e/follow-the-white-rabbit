#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define arr_len(arr) sizeof(arr) / sizeof(arr[0])
#define FALSE 0
#define TRUE 1

#define MAX_WORDS 100000
#define NB_CHARS 16

void print_char2id(int *char2id, int len) {
    printf("------- char2id %d---------\n", len);
    int i;
    int cnt = 0;
    for (i=0; i<len; i++) {
        if (char2id[i] > 0) {
            printf("\t%c\t%d\n", i+'a', char2id[i]);
            cnt++;
        }
    }
    printf("count: %d\n", cnt);
}

int init_char2id(int *char2id, char *id2char, int len, char *anagram, int ana_len) {
    int i;
    int id = 1;
    int idx = -1;

    for (i=0; i<len; i++) {
        char2id[i] = 0;
    }

    for (i=0; i<ana_len; i++) {
        if (anagram[i] >= 'a' && anagram[i] <= 'z') {
            idx = anagram[i] - 'a';
            if (char2id[idx] <= 0) {
                char2id[idx] = id;
                id2char[id] = anagram[i];
                id++;
            }
        }
    }
    return id;
};

char **load_wordlist(char *fpath, int *p_cnt) {
    FILE *file = fopen(fpath, "r");
    char **lines = malloc(MAX_WORDS * sizeof(char*));
    char *currline = malloc(255);

    assert(file != NULL);

    ssize_t readed;
    size_t len = 255;
    int lineno = 0;
    int buflen = 0;
    while (fgets(currline, len, file) != NULL) {
        //printf("%s, %d\n", currline, len);
        //assert(len <= 255);
        int slen = strlen(currline);
        assert(slen < 255);
        char *p = currline + slen - 1;
        while (*p == '\r' || *p == '\n') {
            *p-- = '\0';;
            slen--;
        }
        buflen = (slen + 8) & (~0x3);
        //printf("%d\n", (slen + 8) & (~0x3));
        assert(buflen >= slen + 2);
        lines[lineno] = malloc(buflen * sizeof(char));
        strcpy(lines[lineno] + 1, currline);
        *lines[lineno] = slen;
        assert (slen <= 255);
        //printf("got line [%6d] [%3d/%3d/%3d]: %s\n", lineno, *lines[lineno], len, readed, lines[lineno]+1);
        /* Do something with `currentline` */
        lineno++;
    }

    (void)fclose(file);
    *p_cnt = lineno;
    return lines;
}

int squeeze(char **words, int nb_words) {
    int i = 0;
    int j = 0;
    for (j=0; j<nb_words; j++) {
        if (words[j] != NULL) {
            words[i++] = words[j];
            //printf("%d --> %d\n", j, i);
        }
    }
    return i;
}

int word2id(char *word, int len, int *char2id) {
    int i;
    int id;
    for (i=0; i<len; i++) {
        if (word[i] < 'a' || word[i] > 'z') {
            return FALSE;
        }
        id = char2id[word[i] - 'a'];
        if (id <= 0) {
            return FALSE;
        }
        word[i] = id;
    }
    return TRUE;
}

void id2word(char *buf, char *ids, int len, char *id2char) {
    char *p = ids;
    char *dst = buf;
    while (p - ids < len) {
        *dst++ = id2char[*p++];
    }
    *dst = '\0';
}

int words2id(char **words, int nb_words, int *char2id) {
    int i;
    int flg;
    for (i=0; i<nb_words; i++) {
        //printf("%d word:[%x]\n", i, words[i]);
        flg = word2id(words[i]+1, *words[i], char2id);
        if (!flg) {
            words[i] = NULL;
        } else {
            //printf("%d valid word:[%d]\n", i, *words[i]);
        }
    }

    return squeeze(words, nb_words);
}


void print_words_id(char **words, int nb_vw, char *id2char) {
    int i;
    int j;
    int len;
    char buf[255];
    for (i=0; i<nb_vw; i++) {
        len = words[i][0];
        printf("\t%d\t", len);
        for (j=0; j<len; j++) {
            printf("%3d", words[i][j+1]);
        }
        id2word(buf, words[i]+1, len, id2char);
        printf("\t%s", buf);
        printf("\n");
    }
}

char *create_cnt(char *ids, int len, char *id2char) {
    char *cnt = malloc(NB_CHARS * sizeof(char));
    int i = 0;
    int idx = 0;
    for (i=0; i<NB_CHARS; i++) {
        cnt[i] = 0;
    }
    for (i=0; i<len; i++) {
        idx = ids[i];
        if (idx == -1) {
            continue;
        }
        assert(idx >= 0);
        assert(idx <= 26);
        printf("id:%d, char:%c\n", idx, id2char[idx]);
        cnt[idx]++;
        assert(cnt[idx] < 255);
    }
    return cnt;
}

void print_cnt(char *cnt, int max, char *id2char) {
    int i;
    printf("cnt: %d .. %d\n", 1, max-1);
    for (i=1; i<max; i++) {
        printf("\t%c\t%d\n", id2char[i], cnt[i]);
    }
}

int main() {
    char anagram[] = "poultry outwits ants";
    printf("%s\n", anagram);

    int char2id[32] = {0};
    char id2char[32] = {0};

    int ana_len = arr_len(anagram);

    int i;
    int id = 0;
    int idx = -1;

    int max_id = init_char2id(char2id, id2char, arr_len(char2id), anagram, ana_len);
    print_char2id(char2id, arr_len(char2id));

    int nb_words;
    char **words = load_wordlist("./worduniq", &nb_words);
    printf("--- got %d words ---\n", nb_words);

    int nb_vw = words2id(words, nb_words, char2id);
    printf("--- got %d valid words ---\n", nb_vw);
    //print_words_id(words, nb_vw, id2char);

    char ana_ids[arr_len(anagram)];
    char c;
    for (i=0; i<ana_len; i++) {
        c = anagram[i];
        if (c >= 'a' && c <= 'z') {
            idx = anagram[i] - 'a';
            ana_ids[i] = char2id[idx];
        } else {
            ana_ids[i] = -1;
        }
    }
    char *ana_cnt = create_cnt(ana_ids, ana_len, id2char);
    print_cnt(ana_cnt, max_id, id2char);

    char **create_cnt_words(
    return 0;
}



